#-*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.tools import datetime_strftime
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date

__all__ = ['Appointment']



class Appointment(metaclass=PoolMeta):
    'Appointments'
    __name__ = 'gnuhealth.appointment'
    edad = fields.Function(fields.Char('Edad'), 'get_edad')    
    sexo = fields.Function(fields.Char('Sexo'), 'get_sexo')

    def get_edad(self, patient):
        res = ''
        if self.patient:
            res = self.patient.age
        return res

    def get_sexo(self, patient):
        res = ''
        if self.patient:
            res = self.patient.gender
        return res

