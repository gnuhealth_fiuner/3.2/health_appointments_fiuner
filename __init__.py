# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health_appointments import *


def register():
    Pool.register(
        Appointment,
        module='health_appointments_fiuner', type_='model')

